# Qron

Qron is a simple GUI using [python3-qappconfig](https://gitlab.com/juanma1980/python3-appconfig) for manage cron and at jobs.

With Qron you can see the scheduled tasks (both from cron and at) sorted by remaining time for launch, edit them or add new tasks.
